// GENERATED AUTOMATICALLY FROM 'Assets/Data/Input/InputControls.inputactions'

using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class InputControls : IInputActionCollection
{
    private InputActionAsset asset;
    public InputControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputControls"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""092fd5c0-909c-4263-8ccb-2927d0d2eb14"",
            ""actions"": [
                {
                    ""name"": ""MainAction"",
                    ""type"": ""Value"",
                    ""id"": ""4ecb492d-0cad-48d2-a8f1-33d1585a2d29"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SecondAction"",
                    ""type"": ""Button"",
                    ""id"": ""47ee1413-6db6-42c2-82af-0ac5b7653b12"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Tablet"",
                    ""type"": ""Value"",
                    ""id"": ""6b4da6e1-7424-4c79-a6ff-e946ae0d2e6d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Flashlight"",
                    ""type"": ""Value"",
                    ""id"": ""d685c28e-9e1d-4795-a5a3-14e03714f9b5"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Movement"",
                    ""type"": ""Value"",
                    ""id"": ""0c809662-7aa6-497e-a830-b0062d85f9a0"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Value"",
                    ""id"": ""23d09540-26fc-41b4-86a9-77ca88d442ea"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Crouch"",
                    ""type"": ""Value"",
                    ""id"": ""f8a4cb6b-50c3-45de-bd8a-3273cbd7d1cb"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Run"",
                    ""type"": ""Button"",
                    ""id"": ""129e0c3c-b6c8-40e4-b94f-dd0f25991cac"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""Value"",
                    ""id"": ""c19d651c-8762-4962-84f5-af064a42816b"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SwitchMenu"",
                    ""type"": ""Value"",
                    ""id"": ""6b8bc097-885b-4475-a203-cc49301b398a"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""4ace39aa-28be-4b05-b04f-371581106d19"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""MainAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4b4e699c-037a-4584-8f0a-5fcf9cd22050"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""SecondAction"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5cfd6b0e-fa21-42a7-8662-37ed23de5534"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""Tablet"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""57c3a6ef-0ab2-40db-bb39-690962851199"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""Flashlight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Dpad"",
                    ""id"": ""c2846c71-a935-4b92-9806-4e338dbada51"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""017bf8a0-9fbf-4f35-b226-51df38f7f788"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""37724185-9680-480e-bbb1-ba39872778cc"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""6b5c2f9f-ab7a-47c7-83ae-313138a06fba"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""443e287c-6b04-4dba-b65f-3e9e3e34050c"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""b1b09c3b-1c8b-4d73-9d88-f2960416f260"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""239e45d8-910f-4fb1-bc6f-fcfe229ce61b"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""Crouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""11a91009-5a1d-4ae1-bb81-65e6d54a23dc"",
                    ""path"": ""<Keyboard>/c"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""Crouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dbcd3826-620f-47d0-9726-194c39b284c9"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""Run"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Up"",
                    ""id"": ""b569b647-1f53-4d96-a624-cc60c9f271f9"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Look"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""2105754c-9702-4129-af38-84c5584c3b48"",
                    ""path"": ""<Mouse>/delta/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""9b9fbc2c-d5ed-4ea2-b594-7ee6d532dccd"",
                    ""path"": ""<Mouse>/delta/y"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""6206002f-4b04-4797-8a1a-dc2ca6bc8b2f"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""SwitchMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7a28de60-7a56-493a-8e15-be91250d5df6"",
                    ""path"": ""<Keyboard>/p"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";MouseAndKeyboard"",
                    ""action"": ""SwitchMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""MouseAndKeyboard"",
            ""basedOn"": """",
            ""bindingGroup"": ""MouseAndKeyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""basedOn"": """",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Player
        m_Player = asset.GetActionMap("Player");
        m_Player_MainAction = m_Player.GetAction("MainAction");
        m_Player_SecondAction = m_Player.GetAction("SecondAction");
        m_Player_Tablet = m_Player.GetAction("Tablet");
        m_Player_Flashlight = m_Player.GetAction("Flashlight");
        m_Player_Movement = m_Player.GetAction("Movement");
        m_Player_Jump = m_Player.GetAction("Jump");
        m_Player_Crouch = m_Player.GetAction("Crouch");
        m_Player_Run = m_Player.GetAction("Run");
        m_Player_Look = m_Player.GetAction("Look");
        m_Player_SwitchMenu = m_Player.GetAction("SwitchMenu");
    }

    ~InputControls()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_MainAction;
    private readonly InputAction m_Player_SecondAction;
    private readonly InputAction m_Player_Tablet;
    private readonly InputAction m_Player_Flashlight;
    private readonly InputAction m_Player_Movement;
    private readonly InputAction m_Player_Jump;
    private readonly InputAction m_Player_Crouch;
    private readonly InputAction m_Player_Run;
    private readonly InputAction m_Player_Look;
    private readonly InputAction m_Player_SwitchMenu;
    public struct PlayerActions
    {
        private InputControls m_Wrapper;
        public PlayerActions(InputControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @MainAction => m_Wrapper.m_Player_MainAction;
        public InputAction @SecondAction => m_Wrapper.m_Player_SecondAction;
        public InputAction @Tablet => m_Wrapper.m_Player_Tablet;
        public InputAction @Flashlight => m_Wrapper.m_Player_Flashlight;
        public InputAction @Movement => m_Wrapper.m_Player_Movement;
        public InputAction @Jump => m_Wrapper.m_Player_Jump;
        public InputAction @Crouch => m_Wrapper.m_Player_Crouch;
        public InputAction @Run => m_Wrapper.m_Player_Run;
        public InputAction @Look => m_Wrapper.m_Player_Look;
        public InputAction @SwitchMenu => m_Wrapper.m_Player_SwitchMenu;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                MainAction.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMainAction;
                MainAction.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMainAction;
                MainAction.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMainAction;
                SecondAction.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSecondAction;
                SecondAction.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSecondAction;
                SecondAction.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSecondAction;
                Tablet.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnTablet;
                Tablet.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnTablet;
                Tablet.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnTablet;
                Flashlight.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFlashlight;
                Flashlight.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFlashlight;
                Flashlight.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFlashlight;
                Movement.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                Movement.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                Movement.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                Jump.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                Jump.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                Jump.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                Crouch.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCrouch;
                Crouch.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCrouch;
                Crouch.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCrouch;
                Run.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRun;
                Run.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRun;
                Run.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRun;
                Look.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLook;
                Look.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLook;
                Look.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLook;
                SwitchMenu.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSwitchMenu;
                SwitchMenu.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSwitchMenu;
                SwitchMenu.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSwitchMenu;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                MainAction.started += instance.OnMainAction;
                MainAction.performed += instance.OnMainAction;
                MainAction.canceled += instance.OnMainAction;
                SecondAction.started += instance.OnSecondAction;
                SecondAction.performed += instance.OnSecondAction;
                SecondAction.canceled += instance.OnSecondAction;
                Tablet.started += instance.OnTablet;
                Tablet.performed += instance.OnTablet;
                Tablet.canceled += instance.OnTablet;
                Flashlight.started += instance.OnFlashlight;
                Flashlight.performed += instance.OnFlashlight;
                Flashlight.canceled += instance.OnFlashlight;
                Movement.started += instance.OnMovement;
                Movement.performed += instance.OnMovement;
                Movement.canceled += instance.OnMovement;
                Jump.started += instance.OnJump;
                Jump.performed += instance.OnJump;
                Jump.canceled += instance.OnJump;
                Crouch.started += instance.OnCrouch;
                Crouch.performed += instance.OnCrouch;
                Crouch.canceled += instance.OnCrouch;
                Run.started += instance.OnRun;
                Run.performed += instance.OnRun;
                Run.canceled += instance.OnRun;
                Look.started += instance.OnLook;
                Look.performed += instance.OnLook;
                Look.canceled += instance.OnLook;
                SwitchMenu.started += instance.OnSwitchMenu;
                SwitchMenu.performed += instance.OnSwitchMenu;
                SwitchMenu.canceled += instance.OnSwitchMenu;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);
    private int m_MouseAndKeyboardSchemeIndex = -1;
    public InputControlScheme MouseAndKeyboardScheme
    {
        get
        {
            if (m_MouseAndKeyboardSchemeIndex == -1) m_MouseAndKeyboardSchemeIndex = asset.GetControlSchemeIndex("MouseAndKeyboard");
            return asset.controlSchemes[m_MouseAndKeyboardSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.GetControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface IPlayerActions
    {
        void OnMainAction(InputAction.CallbackContext context);
        void OnSecondAction(InputAction.CallbackContext context);
        void OnTablet(InputAction.CallbackContext context);
        void OnFlashlight(InputAction.CallbackContext context);
        void OnMovement(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnCrouch(InputAction.CallbackContext context);
        void OnRun(InputAction.CallbackContext context);
        void OnLook(InputAction.CallbackContext context);
        void OnSwitchMenu(InputAction.CallbackContext context);
    }
}
