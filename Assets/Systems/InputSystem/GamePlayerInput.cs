﻿using System;
using UnityEngine;

public class GamePlayerInput : MonoBehaviour
{
    //NOTE:  MainAction MUST have PRESS AND RELEASE Interaction
    public event Action MainActionStarted;
    public event Action MainActionPerformed;
    
    public event Action SecondActionStarted;
    public event Action SecondActionPerformed;
    public event Action SwitchFlashLightPerformed;
    
    public event Action SwitchTabletPerformed;
    public event Action SwitchMenuPerformed;
    
    public event Action<Vector2> MovementPerformed;
    public event Action<Vector2> MovementCancelled;
    
    public event Action<Vector2> LookPerformed;
    
    public event Action JumpPerformed;
    public event Action CrouchPerformed;
    
    public event Action RunStarted;
    public event Action RunPerformed;

    public InputControls Controls;


    private void Awake()
    {
        Controls = new InputControls();
        //Actions
        Controls.Player.MainAction.started += context => MainActionStarted?.Invoke();
        Controls.Player.MainAction.canceled += context => MainActionPerformed?.Invoke();
        Controls.Player.SecondAction.performed += context => SecondActionPerformed?.Invoke();
        Controls.Player.Flashlight.performed += context => SwitchFlashLightPerformed?.Invoke();
        Controls.Player.Tablet.performed += context => SwitchTabletPerformed?.Invoke();
        Controls.Player.SwitchMenu.performed += context => SwitchMenuPerformed?.Invoke();
        //Move
        Controls.Player.Movement.performed += context => MovementPerformed?.Invoke(context.ReadValue<Vector2>());
        Controls.Player.Movement.canceled += context => MovementCancelled?.Invoke(context.ReadValue<Vector2>());
        //Look
        /*Controls.Player.Look.performed += context => LookPerformed?.Invoke(context.ReadValue<Vector2>());*/
        //Jump
        Controls.Player.Jump.performed += context => JumpPerformed?.Invoke();
        Controls.Player.Crouch.performed += context => CrouchPerformed?.Invoke();
        
        Controls.Player.Run.started += context => RunStarted?.Invoke();
        Controls.Player.Run.canceled += context => RunPerformed?.Invoke();


        Controls.Enable();
    }
    
    private void OnDestroy()
    {
        Controls.Disable();
    }
}
