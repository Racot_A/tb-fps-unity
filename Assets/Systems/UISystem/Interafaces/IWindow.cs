﻿public interface IWindow
{
    bool IsVisible { get; }
    
    void Show();

    void Hide();

    bool CanShow();

    bool CanHide();

    T GetPanel<T>() where T : class, IPanel;
    void SwitchToPanel(BaseUIPanel targetPanel);
}
