﻿public interface IElement<T>
{
    T Data { get; }
    
    void Initialize(T data);
    void DestroyElement();
    
    void UpdateContent();
}
