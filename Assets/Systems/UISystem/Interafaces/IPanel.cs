﻿public interface IPanel
{
    UIState DefaultState { get; }
    IWindow Owner { get; }
    bool IsVisible { get; }
    
    void Show();

    void Hide();

    bool CanShow();

    bool CanHide();
    
    
}
