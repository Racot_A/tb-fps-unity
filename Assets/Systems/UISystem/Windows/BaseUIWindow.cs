﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum UIState
{
    Show,
    Hide
}

public abstract class BaseUIWindow : MonoBehaviour, IWindow
{
    [SerializeField] private Canvas canvas;
    [SerializeField] private UIState defaultState;

    public UIState DefaultState => defaultState;

    public bool IsVisible { get; private set; }

    public void Show()
    {
        if(!CanShow()) { return; }
        
        OnShow();
    }

    public void Hide()
    {
        if(!CanHide()) { return; }
        
        OnHide();
    }

    public virtual bool CanShow()
    {
        return !IsVisible;
    }
    
    public virtual bool CanHide()
    {
        return IsVisible;
    }

    protected virtual void OnShow()
    {
        canvas.worldCamera = Camera.main;
        canvas.enabled = true;
        
        IsVisible = true;

        var panels = GetPanels();

        foreach (var panel in panels)
        {
            if (panel.DefaultState == UIState.Show)
            {
                panel.Show();
            }
            else
            {
                panel.Hide();
            }
        }
    }
    
    protected virtual void OnHide()
    {
        canvas.enabled = false;
        
        IsVisible = false;
    }
    
    public T GetPanel<T>() where T : class, IPanel
    {
        var panels = GetComponentsInChildren<T>(true);
        if (panels.Length <= 0){ return default(T); }

        return Array.Find(panels, x => x != null) as T;
    }
    
    private List<IPanel> GetPanels()
    {
        var panels = GetComponentsInChildren<IPanel>(true);
        return new List<IPanel>(panels);
    }

    public void SwitchToPanel(BaseUIPanel targetPanel)
    {
        var panels = GetPanels();

        foreach (var panel in panels)
        {
            if (!panel.Equals(targetPanel))
            {
                panel.Hide();
            }
            else
            {
                targetPanel.Show();
            }
        }
    }
}
