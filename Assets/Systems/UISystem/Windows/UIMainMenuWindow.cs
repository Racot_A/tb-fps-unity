﻿using UnityEngine;

public class UIMainMenuWindow : BaseUIWindow
{
    [SerializeField] private UIDisclaimerPanel disclaimerPanel;
    private void Start()
    {
        SwitchToPanel(disclaimerPanel);
    }

}
