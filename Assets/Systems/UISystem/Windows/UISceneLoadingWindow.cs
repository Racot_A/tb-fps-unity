﻿using TMPro;
using UnityEngine;

public class UISceneLoadingWindow : BaseUIWindow
{
    [SerializeField] private CanvasGroup fadeGroup;
    [SerializeField] private TextMeshProUGUI loadingText;
    public CanvasGroup FadeCanvasGroup => fadeGroup;

    private void Awake()
    {
        SetLoadingText(string.Empty);
    }

    public void SetLoadingText(string text)
    {
        loadingText.SetText(text);
    }
}