﻿using System.Collections;
using UnityEngine;

public class UIDisclaimerPanel : BaseUIPanel
{
    private Coroutine _anyKeyCor;
    
    protected override void OnShow()
    {
        base.OnShow();

        _anyKeyCor = StartCoroutine(WaitAnyKey());
    }

    protected override void OnHide()
    {
        if (_anyKeyCor != null)
        {
            StopCoroutine(_anyKeyCor);
            _anyKeyCor = null;
        }
        
        base.OnHide();
        
        var optionsPanel = Owner.GetPanel<UIMainMenuOptionsPanel>();
        Owner.SwitchToPanel(optionsPanel);
    }

    IEnumerator WaitAnyKey()
    {
        yield return new WaitUntil(() => Input.anyKeyDown);
        
        Hide();
    }    
}


