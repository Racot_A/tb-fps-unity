﻿using TMPro;
using UnityEngine;

public class UIScoresPanel : BaseUIPanel
{
    [SerializeField] private TextMeshProUGUI scoreValueText;

    private GameSaveData _gameSaveData;

    private void Start()
    {
        ScoreOnScoreUpdateEvent(Core.Instance.References.Score.Score);
        
        Core.Instance.References.Score.ScoreUpdateEvent += ScoreOnScoreUpdateEvent;
    }
    
    private void OnDestroy()
    {
        Core.Instance.References.Score.ScoreUpdateEvent -= ScoreOnScoreUpdateEvent;
    }
    
    private void ScoreOnScoreUpdateEvent(int score)
    {
        scoreValueText.SetText($"{score}");
    }

}
