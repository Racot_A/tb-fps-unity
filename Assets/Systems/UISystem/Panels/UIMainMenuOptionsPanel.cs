﻿using UnityEngine;
using UnityEngine.UI;

public class UIMainMenuOptionsPanel : BaseUIPanel
{
    [SerializeField] private Button startButton;
    [SerializeField] private Button optionsButton;
    [SerializeField] private Button quitButton;

    private UISettingsPanel _settingsPanel;
    
    private void Start()
    {
        startButton.onClick.AddListener(StartGame);
        optionsButton.onClick.AddListener(ShowSettings);
        quitButton.onClick.AddListener(QuitFromGame);
        
        _settingsPanel =  Owner.GetPanel<UISettingsPanel>();
        _settingsPanel.HideEvent += Show;

        var sceneHandler = SceneHandler.Current as MenuSceneHandler;
        sceneHandler.LoadBackgroundScene();
    }

    private void StartGame()
    {
        Core.Instance.References.ScenesLoader.LoadGame();
    }
    
    private void ShowSettings()
    {
        Owner.SwitchToPanel(_settingsPanel);
    }
    
    private void QuitFromGame()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }
}
