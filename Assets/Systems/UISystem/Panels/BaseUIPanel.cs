﻿using System;
using UnityEngine;

public class BaseUIPanel : MonoBehaviour, IPanel
{
    public event Action ShowEvent;
    public event Action HideEvent;
    
    [SerializeField] private UIState defaultState;

    public UIState DefaultState => defaultState;

    public IWindow Owner => GetComponentInParent<IWindow>();
    
    public bool IsVisible { get; private set; }

    public void Show()
    {
        if(!CanShow()) { return; }
        
        OnShow();
        
        ShowEvent?.Invoke();
    }

    public void Hide()
    {
        if(!CanHide()) { return; }
        
        OnHide();
        
        HideEvent?.Invoke();
    }

    public virtual bool CanShow()
    {
        return !IsVisible;
    }
    
    public virtual bool CanHide()
    {
        return IsVisible;
    }

    protected virtual void OnShow()
    {
        gameObject.SetActive(true);
        
        IsVisible = true;
    }
    
    protected virtual void OnHide()
    {
        gameObject.SetActive(false);
        
        IsVisible = false;
    }

    
}
