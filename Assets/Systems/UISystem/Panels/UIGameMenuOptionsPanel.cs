﻿using UnityEngine;
using UnityEngine.UI;

public class UIGameMenuOptionsPanel : BaseUIPanel
{
    [SerializeField] private Button resumeButton;
    [SerializeField] private Button optionsButton;
    [SerializeField] private Button quitButton;

    private UISettingsPanel _settingsPanel;
    
    private void Start()
    {
        resumeButton.onClick.AddListener(ResumeGame);
        optionsButton.onClick.AddListener(ShowSettings);
        quitButton.onClick.AddListener(QuitFromGame);
        
        _settingsPanel = Owner.GetPanel<UISettingsPanel>();
        _settingsPanel.HideEvent += Show;
    }

    private void ResumeGame()
    {
        var sceneHandler = SceneHandler.Current as GameSceneHandler;
        sceneHandler.ResumeGame();
    }
    
    private void ShowSettings()
    {
        Owner.SwitchToPanel(_settingsPanel);
    }
    
    private void QuitFromGame()
    {
        Core.Instance.References.ScenesLoader.LoadMenu();
    }
}

