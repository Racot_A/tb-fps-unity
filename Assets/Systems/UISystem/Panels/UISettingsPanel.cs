﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UISettingsPanel : BaseUIPanel
{
    //Move
    [SerializeField] private Slider playerMoveSpeedSlider;
    [SerializeField] private TextMeshProUGUI playerMoveSpeedSliderText;
    [Space(10)]
    //Run
    [SerializeField] private Slider playerRunSpeedSlider;
    [SerializeField] private TextMeshProUGUI playerRunSpeedSliderText;
    [Space(10)]
    //Back button
    [SerializeField] private Button backButton;

    private PlayerSaveData _playerSaveData;

    private void Start()
    {
        backButton.onClick.AddListener(Hide);

        _playerSaveData = Core.Instance.References.Saves.DataContainer.Player;

        playerMoveSpeedSlider.value = _playerSaveData.MovementSpeed;
        playerRunSpeedSlider.value = _playerSaveData.RunningSpeed;

        playerMoveSpeedSlider.onValueChanged.AddListener(MoveSpeedSliderValueChanged);
        playerRunSpeedSlider.onValueChanged.AddListener(RunSpeedSliderValueChanged);
        
        UpdateSlidersText();
    }

    private void MoveSpeedSliderValueChanged(float value)
    {
        _playerSaveData.MovementSpeed = value;
        
        UpdateSlidersText();
    }
    
    private void RunSpeedSliderValueChanged(float value)
    {
        _playerSaveData.RunningSpeed = value;
        
        UpdateSlidersText();
    }

    private void UpdateSlidersText()
    {
        playerMoveSpeedSliderText.SetText($"{playerMoveSpeedSlider.value:F0}");
        playerRunSpeedSliderText.SetText($"{playerRunSpeedSlider.value:F0}");
    }
}
