﻿[System.Serializable]
public class SaveDataContainer
{
    public PlayerSaveData Player;
    public GameSaveData Game;
}
