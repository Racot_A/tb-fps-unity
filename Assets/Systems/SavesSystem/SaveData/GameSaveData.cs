﻿using UnityEngine;

public class GameSaveData : SaveData
{
    [SerializeField] private int maxScore;
    
    public int MaxScore
    {
        get { return maxScore; }
        set
        {
            if (maxScore != value) { Modify(); }
            maxScore = Mathf.Max(maxScore, value);
        }
    }
}
