﻿using UnityEngine;

[System.Serializable]
public class PlayerSaveData : SaveData
{
    [SerializeField] private float movementSpeed = 2;
    [SerializeField] private float runningSpeed = 5;

    public float MovementSpeed
    {
        get { return movementSpeed; }
        set
        {
            if (movementSpeed != value) { Modify(); }
            movementSpeed = value;
        }
    }

    public float RunningSpeed 
    {
        get { return runningSpeed; }
        set
        {
            if (runningSpeed != value) { Modify(); }
            runningSpeed = value;
        }
    }
}
