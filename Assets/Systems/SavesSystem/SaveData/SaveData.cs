﻿using System;

[System.Serializable]
public class SaveData
{
    public event Action ModifyEvent;

    protected void Modify()
    {
        ModifyEvent?.Invoke();
    }
}
