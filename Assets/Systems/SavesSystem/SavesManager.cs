﻿using System.IO;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Core/SavesManager")]
public class SavesManager : ScriptableObject
{
    public SaveDataContainer DataContainer = new SaveDataContainer();

    public string SavesPath => Application.persistentDataPath;
    public string SavesFileName => "LocalSaves.json";
    
    public void Initialize()
    {
        LoadProgress();
    }
    public void Deinitialize()
    {
        SaveProgress();
    }
    
    public void SaveProgress()
    {
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        if (!di.Exists)
        {
            Debug.LogError("Save Directory NOT Exist");
            return;
        }
        
        var path = Path.Combine(SavesPath, SavesFileName);
        
        //Convert to Json and Encrypt
        var jsonText = JsonUtility.ToJson(DataContainer, true);
        jsonText = EncryptString.StringCipher.Encrypt(jsonText, SavesConstants.SavesEncryptKey);
        //Write to file
        File.WriteAllText(path, jsonText);
        
        #if UNITY_EDITOR
            Debug.LogWarning("Save game to path:" + path);
        #endif
    }

    public void LoadProgress()
    {
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        var path = Path.Combine(SavesPath, SavesFileName);
        
        if (di.Exists && File.Exists(path))
        {
            //Convert to Json and Decrypt
            var jsonText = File.ReadAllText(path);
            jsonText = EncryptString.StringCipher.Decrypt(jsonText, SavesConstants.SavesEncryptKey);
            //Init data from json
            DataContainer = JsonUtility.FromJson<SaveDataContainer>(jsonText);
            
            #if UNITY_EDITOR
                 Debug.LogWarning("Load game from path:" + path);
            #endif
        }
    }
}
