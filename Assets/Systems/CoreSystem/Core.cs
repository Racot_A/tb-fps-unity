﻿/*
 * Core is main class used for init all systems
 * */

using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CoreReferences))]
public class Core : MonoBehaviour
{
    public static Core Instance;

    [SerializeField] private CoreReferences references;
    
    public CoreReferences References => references;
    private void Awake()
    {
        Initialize();
    }

    private void OnApplicationQuit()
    {
        DeinitializeSystems();
    }

    private void Initialize()
    {
        Instance = this;

        InitializeSystems();

        Application.targetFrameRate = 60;
        
        DontDestroyOnLoad(this.gameObject);
    }

    private void InitializeSystems()
    {
        references.ScenesLoader.Initialize();
        references.Saves.Initialize();
        references.Score.Initialize();
    }
    
    private void DeinitializeSystems()
    {
        references.ScenesLoader.Deinitialize();
        references.Saves.Deinitialize();
        references.Score.Deinitialize();
    }

    public Coroutine Wait(float duration, Action complete)
    {
        return StartCoroutine(WaitCor(duration, complete));
    }
    
    public void StopStartedCoroutine(Coroutine coroutine)
    {
        if (coroutine == null) { return; }
        
        StopCoroutine(coroutine);
    }
    public Coroutine WaitWhile(Func<bool> predicate, Action complete)
    {
        return StartCoroutine(WaitWhileCor(predicate, complete));
    }

    IEnumerator WaitCor(float duration, Action complete)
    {
        yield return new WaitForSeconds(duration);
        complete?.Invoke();
    }
    
    IEnumerator WaitWhileCor(Func<bool> predicate, Action complete)
    {
        yield return new WaitWhile(predicate);
        complete?.Invoke();
    }
}