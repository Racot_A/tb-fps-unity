﻿using UnityEngine;

public class MenuSceneHandler : SceneHandler
{
    [SerializeField] private string backgroundScene;

    public void LoadBackgroundScene()
    {
        Core.Instance.References.ScenesLoader.LoadBackgroundScene(backgroundScene);
    }
}
