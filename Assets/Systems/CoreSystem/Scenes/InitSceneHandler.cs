﻿public class InitSceneHandler : SceneHandler
{
    protected override void Start()
    {
        base.Start();
        
        Core.Instance.References.ScenesLoader.LoadMenu();
    }
}
