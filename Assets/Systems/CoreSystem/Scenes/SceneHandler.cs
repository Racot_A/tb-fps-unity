﻿/*
 * base class for scenes. Handle and have some king of logic
 */

using UnityEngine;

public class SceneHandler : MonoBehaviour
{
    #region Static Params

    public static Camera MainCamera
    {
        get
        {
            if (!_mainCamera)
            {
                _mainCamera = Camera.main;
            }

            return _mainCamera;
        }
    }
    
    public static SceneHandler Current
    {
        get
        {
            if (!_sceneHandler)
            {
                _sceneHandler = FindObjectOfType<SceneHandler>();
            }

            return _sceneHandler;
        }
    }

    private static Camera _mainCamera;
    private static SceneHandler _sceneHandler;
    #endregion
    
    protected virtual void Awake()
    {
        if (Core.Instance == null)
        {
            Instantiate(Resources.Load<Core>("Core"));
        }
    }
    
    protected virtual void Start()
    {
        
    }
}
