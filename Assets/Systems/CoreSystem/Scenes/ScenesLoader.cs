﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "SO/Core/ScenesLoader")]
public class ScenesLoader : ScriptableObject
{
    
    public event Action BeforeSceneUnload;
    public event Action AfterSceneLoad;

    [SerializeField] private float fadeDuration = 1f;
    [Space(10)]
    [SerializeField] private string menuSceneName;
    [SerializeField] private string gameSceneName;

    private bool _isFading;
    private UISceneLoadingWindow _loadingWindow;
    
    public bool Initialized { get; private set; }

    public void Initialize()
    {
        if (Initialized)
        {
            return;
        }
        
        Initialized = true;

        _loadingWindow = Core.Instance.References.LoadingWindow;
    }
    
    public void Deinitialize()
    {
        Initialized = false;
        _isFading = false;
    }
    
    public void FadeAndLoadScene(string sceneName)
    {
        if (_isFading)
        {
            return;
        }

        Time.timeScale = 1;
        
        Core.Instance.StartCoroutine(FadeAndSwitchScenes(sceneName));
    }
    
    public void LoadScene(string sceneName)
    {
        Time.timeScale = 1;
        Core.Instance.StartCoroutine(SwitchScenes(sceneName));
    }

    private IEnumerator FadeAndSwitchScenes(string sceneName)
    {
        Core.Instance.References.LoadingWindow.Show();
        yield return Core.Instance.StartCoroutine(Fade(1));
        BeforeSceneUnload?.Invoke();
        yield return Core.Instance.StartCoroutine(LoadSceneAndSetActive(sceneName, LoadSceneMode.Single));
        AfterSceneLoad?.Invoke();

        yield return Core.Instance.StartCoroutine(Fade(0));
        
        Core.Instance.References.LoadingWindow.Hide();
    }
    
    private IEnumerator SwitchScenes(string sceneName)
    {
        BeforeSceneUnload?.Invoke();
        yield return Core.Instance.StartCoroutine(LoadSceneAndSetActive(sceneName, LoadSceneMode.Single));
        AfterSceneLoad?.Invoke();
    }
    
    private IEnumerator LoadSceneAndSetActive(string sceneName, LoadSceneMode loadSceneMode)
    {
        yield return SceneManager.LoadSceneAsync(sceneName, loadSceneMode);

        Scene newsLyLoadedScene = SceneManager.GetSceneAt(SceneManager.sceneCount - 1);
        SceneManager.SetActiveScene(newsLyLoadedScene);
    }

    private IEnumerator Fade(float finalAlpha)
    {
        var loadingWindow = Core.Instance.References.LoadingWindow;
        var fadeCanvasGroup = loadingWindow.FadeCanvasGroup; 
        _isFading = true;
        fadeCanvasGroup.blocksRaycasts = true;

        var fadeSpeed = Mathf.Abs((fadeCanvasGroup.alpha - finalAlpha) / fadeDuration);
        while (!Mathf.Approximately(fadeCanvasGroup.alpha, finalAlpha))
        {
            fadeCanvasGroup.alpha = Mathf.MoveTowards(fadeCanvasGroup.alpha, finalAlpha, fadeSpeed * Time.deltaTime);
            yield return null;
        }

        _isFading = false;
        fadeCanvasGroup.blocksRaycasts = false;
    }

    public void LoadMenu()
    {
        FadeAndLoadScene(menuSceneName);
    }

    public void LoadGame()
    {
        FadeAndLoadScene(gameSceneName);
    }

    public void LoadBackgroundScene(string backgroundScene)
    {
        Core.Instance.StartCoroutine(LoadSceneAndSetActive(backgroundScene, LoadSceneMode.Additive));
    }
}
