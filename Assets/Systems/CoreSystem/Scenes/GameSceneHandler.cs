﻿using UnityEngine;

public class GameSceneHandler : SceneHandler
{
    [SerializeField] private UIGameMenuWindow gameMenuWindowPrefab;
    
    
    private UIGameMenuWindow _menuWindow;
    
    #region Static Params

    public static PlayerCharacter Player
    {
        get
        {
            if (!_player)
            {
                _player = FindObjectOfType<PlayerCharacter>();
            }

            return _player;
        }
    }

    private static PlayerCharacter _player;
    #endregion
    
    protected override void Start()
    {
        base.Start();
    }
    
    public void PauseGame()
    {
        Time.timeScale = 0;

        if (_menuWindow == null)
        {
            _menuWindow = Instantiate(gameMenuWindowPrefab);
        }
        _menuWindow.transform.SetAsLastSibling();
        _menuWindow.Show();
        
        var playerController = GameSceneHandler.Player.GetComponent<PlayerController>();
        playerController.MouseLook.SetCursorLock(false);
        playerController.LockInput();
    } 

    public void ResumeGame()
    {
        Time.timeScale = 1;

        if (_menuWindow)
        {
            _menuWindow.Hide();
        }
        
        var playerController = GameSceneHandler.Player.GetComponent<PlayerController>();
        playerController.MouseLook.SetCursorLock(true);
        playerController.UnlockInput();
    }

    public bool GameIsPaused()
    {
        return Time.time < 0.1;
    }

    public void SwitchPause()
    {
        if (GameIsPaused())
        {
            ResumeGame();
        }
        else
        {
            PauseGame();
        }
    }
}
