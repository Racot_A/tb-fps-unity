﻿using UnityEngine;

public class CoreReferences : MonoBehaviour
{
    [SerializeField] private SavesManager saves;
    [SerializeField] private ScenesLoader scenesLoader;
    [SerializeField] private ScoreManager score;
    [SerializeField] private UISceneLoadingWindow loadingWindow;

    public SavesManager Saves => saves;
    public ScenesLoader ScenesLoader => scenesLoader;
    public ScoreManager Score => score;
    public UISceneLoadingWindow LoadingWindow => loadingWindow;
    

}