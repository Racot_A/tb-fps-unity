﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Core/ScoreManager")]
public class ScoreManager : ScriptableObject
{
    public event Action<int> ScoreUpdateEvent; 
    public int Score { get; private set; }
    
    public void Initialize()
    {
        Score = 0;
    }

    public void Deinitialize()
    {
        Score = 0;
    }

    public void AddScore(int amount)
    {
        Score += amount;
        ScoreUpdateEvent?.Invoke(Score);
    }

    public void RemoveScore(int amount)
    {
        Score -= amount;
        ScoreUpdateEvent?.Invoke(Score);
    }
    
    

    
}
