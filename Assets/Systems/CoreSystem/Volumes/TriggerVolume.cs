﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TriggerVolumeEvent : UnityEvent<Collider> {}

public class TriggerVolume : MonoBehaviour, IVolume
{
    public TriggerVolumeEvent TriggerEnterEvent => triggerEnterEvent;
    public TriggerVolumeEvent TriggerExitEvent => triggerExitEvent;
    
    [SerializeField] private bool activeByDefault = true;
    [SerializeField] private bool checkPlayer;
    [SerializeField] private TriggerVolumeEvent triggerEnterEvent;
    [SerializeField] private TriggerVolumeEvent triggerExitEvent;

    public bool IsActive { get; private set; }

    private void Awake()
    {
        if (activeByDefault)
        {
            Activate();
        }
        else
        {
            Deactivate();
        }
    }

    public void Activate()
    {
        IsActive = true;
    }

    public void Deactivate()
    {
        IsActive = false;
    }
    
    private void OnTriggerEnter(Collider coll)
    {
        if(!IsActive) { return; }
        
        if (checkPlayer)
        {
            var player = coll.GetComponent<PlayerCharacter>();
            if(!player) { return; }
        
            triggerEnterEvent?.Invoke(coll);
        }
        else
        {
            triggerEnterEvent?.Invoke(coll);
        }
    }

    private void OnTriggerExit(Collider coll)
    {
        if(!IsActive) { return; }
        
        if (checkPlayer)
        {
            var player = coll.GetComponent<PlayerCharacter>();
            if(!player) { return; }
        
            triggerExitEvent?.Invoke(coll);
        }
        else
        {
            triggerExitEvent?.Invoke(coll);
        }
    }
}
