﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimersExecutor : MonoBehaviour
{
    private static TimersExecutor _instance = null;
 
    public static TimersExecutor Instance {
        get 
        {
            if (_instance == null)
            {
                _instance = new GameObject("TimersExecutor").AddComponent<TimersExecutor>();
            }
            return _instance;
        }
    }

    private void OnDestroy()
    {
        StopAllTimers();
    }

    private Dictionary<Timer, Coroutine> _timers = new Dictionary<Timer, Coroutine>();
    
    public Timer StartTimer(float time, Action doneAction)
    {
        var timer = new Timer(time, Time.time);
        var coroutine = StartCoroutine(TimerCor(time, () =>
        {
            doneAction();
            StopTimer(timer);
        }));
        _timers.Add(timer, coroutine);

        return timer;
    }
    
    public void StopTimer(Timer timer)
    {
        var containsKey = _timers.ContainsKey(timer);
        if (!containsKey) return;
        
        StopCoroutine(_timers[timer]);
        _timers.Remove(timer);
    }

    private void StopAllTimers()
    {
        foreach (var timer in _timers)
        {
            StopCoroutine(_timers[timer.Key]);
        }
        
        _timers.Clear();
    }

    IEnumerator TimerCor(float time, Action done)
    {
        yield return new WaitForSeconds(time);
        done?.Invoke();
    }
    
}

public struct Timer
{
    private float _startTime;
    private float _duration;
    
    public Timer(float startTime, float duration)
    {
        _startTime = startTime;
        _duration = duration;

    }

    private float StopTime => _startTime + _duration;
    public float RemainantTime => StopTime - _startTime;
}

