﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseFSMState : MonoBehaviour, IState
{
    [SerializeField] private string stateId;

    protected IStateMachine StateMachine { get; private set; }
    
    public string ID => stateId;

    public void Initialize(IStateMachine stateMachine)
    {
        StateMachine = stateMachine;
    }

    public void Activate()
    {
        Debug.Log($"Activate {stateId} state", this);
        OnActivate();
    }

    public void Deactivate()
    {
        Debug.Log($"Deactivate {stateId} state", this);
        OnDeactivate();
    }

    protected abstract void OnActivate();
    protected abstract void OnDeactivate();
}
