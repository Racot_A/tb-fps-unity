﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState
{
    string ID { get; }

    void Initialize(IStateMachine stateMachine);
    void Activate();
    void Deactivate();
}
