﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JohnWantOpenBarrierState : BaseFSMState
{
    [SerializeField] private BaseAIController controller;
    [SerializeField] private Transform openBarrierPanel;
    [SerializeField] private float lookSpeed = 10;
    [SerializeField] private JohnIdleState idleState;
    
    private Coroutine _waitCor;
    
    private static readonly int ActionAnimHash = Animator.StringToHash("TAction");

    protected override void OnActivate()
    {
        controller.MoveToTarget(openBarrierPanel, true);
        _waitCor = Core.Instance.WaitWhile(() => controller.IsMoving, MoveDestinationReached);
    }

    private void MoveDestinationReached()
    {
        controller.LookAt(GameSceneHandler.Player.transform.position, lookSpeed);
        _waitCor = Core.Instance.Wait(1, ActionDestroyPanel);
    }

    private void ActionDestroyPanel()
    {
        if (openBarrierPanel)
        {
            controller.LookAt(openBarrierPanel.position, 10);
        }
       
        controller.Animator.SetTrigger(ActionAnimHash);
        _waitCor = Core.Instance.Wait(1, () => StateMachine.SwitchToState(idleState));

    }

    protected override void OnDeactivate()
    {
        Core.Instance.StopStartedCoroutine(_waitCor);
    }
}
