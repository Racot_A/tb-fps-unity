﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class JohnMoveToRandomPointState : BaseFSMState
{
    [SerializeField] private List<Transform> points = new List<Transform>();
    [SerializeField] private BaseAIController controller;
    [SerializeField] private JohnIdleState idleState;

    private Coroutine _waitCor;
    private Transform _lastPoint;
    protected override void OnActivate()
    {
        var randPoint = GetUniqueRandPoint(_lastPoint);
        controller.MoveToPosition(randPoint.position, false);

        _waitCor = Core.Instance.WaitWhile(() => controller.IsMoving, MoveDestinationReached);
    }

    private Transform GetUniqueRandPoint(Transform lastPoint)
    {
        var randPoint = points[UnityEngine.Random.Range(0, points.Count)];
        if (lastPoint != null)
        {
            while (randPoint == lastPoint)
            {
                randPoint = points[UnityEngine.Random.Range(0, points.Count)];
            }
        }

        _lastPoint = randPoint;
        return randPoint;
    }

    private void MoveDestinationReached()
    {
        StateMachine.SwitchToState(idleState);
    }

    protected override void OnDeactivate()
    {
        Core.Instance.StopStartedCoroutine(_waitCor);
    }
}
