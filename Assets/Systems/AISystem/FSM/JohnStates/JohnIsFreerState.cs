﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class JohnIsFreerState : BaseFSMState
{
    [SerializeField] private BaseAIController controller;
    [SerializeField] private float minFollowDistance = 2f;
    [SerializeField] private NavMeshLinkData link;
    protected override void OnActivate()
    {
        controller.NavMeshAgent.stoppingDistance = minFollowDistance;
        controller.StartFollow(GameSceneHandler.Player.transform);
    }

    protected override void OnDeactivate()
    {
        controller.StopFollow();
    }
}
