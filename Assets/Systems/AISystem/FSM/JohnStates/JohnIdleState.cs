﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JohnIdleState : BaseFSMState
{
    [SerializeField] private BaseAIController controller;
    [SerializeField] private float idleTime = 4;
    [SerializeField] private JohnMoveToRandomPointState moveToRandomPointState;
    
    private static readonly int MoveSpeedHash = Animator.StringToHash("MoveSpeed");

    private Coroutine _waitCor;
    
    protected override void OnActivate()
    {
        var randWaitTime = UnityEngine.Random.Range(0, idleTime);
        _waitCor = Core.Instance.Wait(randWaitTime, ActivateNextState);
    }

    private void ActivateNextState()
    {
        StateMachine.SwitchToState(moveToRandomPointState);
    }

    protected override void OnDeactivate()
    {
        Core.Instance.StopStartedCoroutine(_waitCor);
    }
}
