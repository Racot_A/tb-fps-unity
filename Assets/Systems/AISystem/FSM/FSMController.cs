﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMController : MonoBehaviour, IStateMachine
{
    [SerializeField] private BaseFSMState defaultState;
    [SerializeField] private List<BaseFSMState> availableStates = new List<BaseFSMState>();
    
    private List<IState> _states = new List<IState>();
    private IState _currentState;

    private void Awake()
    {
        foreach (var availableState in availableStates)
        {
            _states.Add(availableState);
            availableState.Initialize(this);
        }
    }

    private void Start()
    {
        SwitchToState(defaultState);
    }

    public void SwitchToState(string stateId)
    {
        var nextState = GetState(stateId);

        if (nextState == null)
        {
            Debug.LogError($"State:{stateId} not find", this);
            return;
        }

        SwitchToState(nextState);
    }

    public void SwitchToState(IState state)
    {
        if (state == null)
        {
            Debug.LogError($"State not find", this);
            return;
        }
        
        _currentState?.Deactivate();

        _currentState = state;
        _currentState.Activate();
    }

    private IState GetState(string stateId)
    {
        return _states.Find(x => x.ID.Equals(stateId));
    }
}
