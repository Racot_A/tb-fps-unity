﻿
public interface IStateMachine
{
    void SwitchToState(string stateId);
    void SwitchToState(IState state);
}
