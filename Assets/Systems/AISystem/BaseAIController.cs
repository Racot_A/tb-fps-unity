﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BaseAIController : MonoBehaviour
{
    public Animator Animator => animator;
    public bool IsStopped { get; private set; }
    public bool IsMoving { get; private set; }
    public NavMeshAgent NavMeshAgent => navMeshAgent;
    private static readonly int MoveSpeedHash = Animator.StringToHash("MoveSpeed");
    
    [SerializeField] private NavMeshAgent navMeshAgent;
    [SerializeField] private Animator animator;
    [SerializeField] private float walkSpeed = 1.5f;
    [SerializeField] private float runSpeed = 3f;
    [SerializeField] private float rotationSpeed = 5f;

    private GameObject _testDestinationPoint;

    private Coroutine _moveCoroutine;
    private Coroutine _lookCor;
    private Vector3 _targetPosition;
    private Transform _followTarget;
    private Coroutine _coroutine;

    private void Awake()
    {
        _testDestinationPoint = new GameObject($"{gameObject.name} testDestinationPoint");
        navMeshAgent.updateRotation = false;
    }

    private void LateUpdate()
    {
        RotateNavMesh();
    }

    #region NavMesh

    private void RotateNavMesh()
    {
        if (navMeshAgent.velocity.sqrMagnitude > Mathf.Epsilon)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation,
                Quaternion.LookRotation(navMeshAgent.velocity.normalized), rotationSpeed * Time.deltaTime);
        }
    }
    
    public float GetRichDistance()
    {
        float distance = 0.0f;
        Vector3[] corners = navMeshAgent.path.corners;

        if (corners.Length <= 0)
        {
            return 0;
        }

        for (int c = 0; c < corners.Length - 1; ++c)
        {
            distance += Mathf.Abs((corners[c] - corners[c + 1]).magnitude);
        }

        return distance;
    }

    #endregion
    
    #region Follow

    public void StartFollow(Transform target)
    {
        StopMove();
        StopLook();
        
        _followTarget = target;

        _coroutine = StartCoroutine(FollowTargetCoroutine(_followTarget));
    }

    public void StopFollow()
    {
        if (_coroutine != null)
        {
            StopCoroutine(_coroutine);
        }
    }
    
    private IEnumerator FollowTargetCoroutine(Transform target)
    {
        if(target == null) { yield break; }
        
        _targetPosition = target.position;
        IsMoving = true;
        
        while (true)
        {
            MoveToTarget(target, true);
            yield return _moveCoroutine;
            yield return new WaitWhile(() => 
                Vector3.Distance(transform.position, target.position) < navMeshAgent.stoppingDistance + (navMeshAgent.stoppingDistance * 0.5f));
            
            yield return null;
        }
    }

    #endregion

    #region Look

    public void LookAt(Vector3 lookPoint, float lookSpeed)
    {
        if (IsMoving) { return; }

        StopLook();
        _lookCor = StartCoroutine(LookAtPositionCor(lookPoint, lookSpeed));
    }

    public void StopLook()
    {
        if (_lookCor != null)
        {
            StopCoroutine(_lookCor);
        }
    }

    IEnumerator LookAtPositionCor(Vector3 lookPoint, float lookSpeed)
    {
        Vector3 direction = (lookPoint - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);

        while (transform.rotation != lookRotation)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * lookSpeed);
            yield return null;
        }
    }

    #endregion

    #region Move

    public void MoveToPosition(Vector3 position, bool isRunning)
    {
        StopMove();
        StopLook();
        
        navMeshAgent.speed = GetMoveSpeed(isRunning);

        if (_moveCoroutine != null)
        {
            StopCoroutine(_moveCoroutine);
        }
        _moveCoroutine = StartCoroutine(MoveToPositionCoroutine(position));

        ResumeMove();
    }

    public void MoveToTarget(Transform target, bool isRunning)
    {
        StopMove();
        StopLook();
        
        navMeshAgent.speed = GetMoveSpeed(isRunning);
        
        if (_moveCoroutine != null)
        {
            StopCoroutine(_moveCoroutine);
        }

        _moveCoroutine = StartCoroutine(MoveToTargetCoroutine(target));
        ResumeMove();
    }
    
    private bool IsMoveToDestination()
    {
        return GetRichDistance() > navMeshAgent.stoppingDistance;
    }
    
    public void DestinationReach()
    {
        IsMoving = false;
        StopMove();
    }
    
    public void StopMove()
    {
        navMeshAgent.isStopped = true;
        animator.SetFloat(MoveSpeedHash, -1);

        IsStopped = true;
    }

    public void ResumeMove()
    {
        navMeshAgent.isStopped = false;
        animator.SetFloat(MoveSpeedHash, navMeshAgent.speed);

        IsStopped = false;
    }
    
    public float GetMoveSpeed(bool run)
    {
        return run ? runSpeed : walkSpeed;
    }
    
     
    IEnumerator MoveToPositionCoroutine(Vector3 position)
    {
        _targetPosition = position;
        IsMoving = true; 
        _testDestinationPoint.transform.position = _targetPosition;

        navMeshAgent.SetDestination(_targetPosition);
        yield return new WaitForEndOfFrame();
        yield return new WaitWhile(IsMoveToDestination);
        DestinationReach();
    }

    private IEnumerator MoveToTargetCoroutine(Transform target)
    {
        if(target == null) { yield break; }
        
        _targetPosition = target.position;
        IsMoving = true; 
        
        navMeshAgent.SetDestination(_targetPosition);
        yield return new WaitForEndOfFrame();
        
        while (IsMoveToDestination())
        {
            _targetPosition = target.position;
            
            _testDestinationPoint.transform.position = _targetPosition;
            navMeshAgent.SetDestination(_targetPosition);
            yield return null;
        }
        DestinationReach();
    }

    #endregion
    
    
}