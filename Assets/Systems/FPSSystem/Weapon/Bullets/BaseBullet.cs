﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class BaseBullet : MonoBehaviour, IBullet
{
    [SerializeField] private float damage = 1;
    [SerializeField] private float deactivateDelay;
    [SerializeField] private UnityEvent hitEvent;
   
    
    public float Damage => damage;
    protected IWeapon Weapon { get; private set; }
    
    protected virtual IDamage DamageType => new BaseDamage();
    private List<ColliderHitReceiver> _hitReceivers = new List<ColliderHitReceiver>();

    private Timer _deactivateTimer;
    
    public bool IsActive => gameObject.activeInHierarchy;

    private void Awake()
    {
        RegisterHits();
    }

    private void OnDestroy()
    {
        UnregisterHits();
    }

    private void RegisterHits()
    {
        _hitReceivers.Clear();
        _hitReceivers.AddRange(GetComponentsInChildren<ColliderHitReceiver>());
        foreach (var hitReceiver in _hitReceivers)
        {
            hitReceiver.CollisionEnter += ReceiverCollisionEnter;
        }
    }
    
    private void UnregisterHits()
    {
        foreach (var hitReceiver in _hitReceivers)
        {
            hitReceiver.CollisionEnter -= ReceiverCollisionEnter;
        }
    }

    public void Initialize(IWeapon weapon)
    {
        Weapon = weapon;
    }

    public void Activate(Vector3 position, Quaternion rotation)
    {
        transform.SetPositionAndRotation(position, rotation);
        gameObject.SetActive(true);
        
        OnActivate();

        if (deactivateDelay > 0)
        {
            _deactivateTimer = TimersExecutor.Instance.StartTimer(deactivateDelay, Deactivate);
        }
    }
    
    public void Deactivate()
    {
        OnDeactivate();

        TimersExecutor.Instance.StopTimer(_deactivateTimer);
        gameObject.SetActive(false);
    }

    private void ReceiverCollisionEnter(Collision coll)
    {
        var damageReceiver = coll.collider.GetComponent<IDamageReceiver>();
        damageReceiver?.ApplyDamage(damage, gameObject, gameObject, DamageType);

        OnHit(coll);
        hitEvent?.Invoke();
        
        Deactivate();
    }
    
    protected abstract void OnActivate();
    protected abstract void OnDeactivate();
    protected abstract void OnHit(Collision coll);
}
