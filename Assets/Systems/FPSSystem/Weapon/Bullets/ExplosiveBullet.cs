﻿using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBullet : BaseBullet
{
    [SerializeField] private float moveSpeed = 10;
    [SerializeField] private float explodeRadius = 1;
    [SerializeField] private float explodePower = 10;
    
    protected override IDamage DamageType => new ExplosiveDamage();

    private Collider[] _affectedColliders = new Collider[MAX_AFFECTED_COLLIDERS];
    private const int MAX_AFFECTED_COLLIDERS = 30;

    protected override void OnActivate()
    {
        var direction = SceneHandler.MainCamera.transform.forward;
        var rb = GetComponent<Rigidbody>();
        rb.AddForce(direction * moveSpeed);
    }

    protected override void OnDeactivate()
    {
        var rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
    }

    protected override void OnHit(Collision coll)
    {
        Explode();
    }

    public void Explode()
    {
        var size = Physics.OverlapSphereNonAlloc(transform.position, explodeRadius, _affectedColliders);
        if (size <= 0) { return; }
        
        ApplyDamageToColliders();

        AddExplosionForce();
    }

    private void AddExplosionForce()
    {
        foreach (Collider coll in _affectedColliders)
        {
            if (coll == null)
            {
                continue;
            }

            var rb = coll.GetComponent<Rigidbody>();
            if (rb)
            {
                rb.AddExplosionForce(explodePower, transform.position, explodeRadius, 3.0F);
            }
        }
    }

    private void ApplyDamageToColliders()
    {
        var damageReceivers = GetReceivers(_affectedColliders);
        foreach (var damageReceiver in damageReceivers)
        {
            float proximity = (transform.position - damageReceiver.Position).magnitude;

            var damage = CalculateDamageByProximity(proximity);
            
            damageReceiver.ApplyDamage(damage, gameObject, gameObject, DamageType);
        }
    }

    public float CalculateDamageByProximity(float proximity)
    {
        if (proximity > explodeRadius)
        {
            return 0;
        }

        float effect = 1 - (proximity / explodeRadius);
        var damage = effect * Damage;
        return damage;
    }


    private List<IDamageReceiver> GetReceivers(Collider[] colliders)
    {
        var receivers = new List<IDamageReceiver>();

        foreach (var coll in colliders)
        {
            if(!coll) { continue; }
            
            var receiver = coll.GetComponent<IDamageReceiver>();
            if (receiver != null)
            {
                receivers.Add(receiver);
            }
        }

        return receivers;
    }
}
