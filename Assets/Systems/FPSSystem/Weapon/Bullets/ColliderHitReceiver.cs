﻿using System;
using UnityEngine;

public class ColliderHitReceiver : MonoBehaviour
{
    public event Action<Collision> CollisionEnter; 
    public event Action<Collision> CollisionExit; 
    
    private void OnCollisionEnter(Collision coll)
    {
        CollisionEnter?.Invoke(coll);
    }
    
    private void OnCollisionExit(Collision coll)
    {
        CollisionExit?.Invoke(coll);
    }
}
