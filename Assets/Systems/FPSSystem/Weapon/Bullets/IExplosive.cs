﻿public interface IExplosive
{
    void Explode();
}
