﻿using System.Collections;
using UnityEngine;

public class CommonBullet : BaseBullet
{
    [SerializeField] private float moveSpeed = 10;
    [SerializeField] private LayerMask hitLayer;

    protected override IDamage DamageType => new CommonDamage();
    
    
    private Coroutine _moveCor;
    private Vector3 _moveDirection => SceneHandler.MainCamera.transform.forward;

    private Coroutine _waitCor;

    protected override void OnActivate()
    {
        if (_moveCor != null)
        {
            StopCoroutine(_moveCor);
        }

        _moveCor = StartCoroutine(Move());
        
        //Create ray
        var ray = new Ray(transform.position, _moveDirection);
        RaycastHit raycastHit;
        var hit = Physics.Raycast(ray, out raycastHit, 50, hitLayer);
        if (hit)
        {
            //Calculate hit time and apply damage
            var time = raycastHit.distance / moveSpeed;

            if (_waitCor != null)
            {
                StopCoroutine(_waitCor);
            }
            _waitCor = StartCoroutine(WaitAndHit(time, raycastHit.collider));
        }
    }
    
    protected override void OnDeactivate()
    {
        if (_moveCor != null)
        {
            StopCoroutine(_moveCor);
        }
    }

    protected override void OnHit(Collision coll)
    {
        
    }

    IEnumerator Move()
    {
        var moveDir = _moveDirection;
        while (IsActive)
        {
            transform.Translate((Time.deltaTime * moveSpeed) * moveDir, Space.World);
            yield return null;
        }
    }
    
    IEnumerator WaitAndHit(float waitTime, Collider coll)
    {
        yield return new WaitForSeconds(waitTime);

        if (coll != null)
        {
            var receiver = coll.GetComponent<IDamageReceiver>();
            receiver?.ApplyDamage(Damage, gameObject, gameObject, DamageType);
        }

        yield return null;
        Deactivate();
    }
}
