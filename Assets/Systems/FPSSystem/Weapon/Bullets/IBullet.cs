﻿using UnityEngine;

public interface IBullet
{
    bool IsActive { get; }
    void Initialize(IWeapon weapon);
    void Activate(Vector3 position, Quaternion rotation);
    void Deactivate();
}
