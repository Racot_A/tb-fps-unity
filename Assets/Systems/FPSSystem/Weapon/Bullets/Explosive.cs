﻿using System.Collections.Generic;
using UnityEngine;

public class Explosive : MonoBehaviour, IExplosive
{
    [SerializeField] private float maxDamage = 10;
    [SerializeField] private float explodeRadius = 1;
    [SerializeField] private float explodePower = 10;
    
    
    private Collider[] _affectedColliders = new Collider[MAX_AFFECTED_COLLIDERS];
    private const int MAX_AFFECTED_COLLIDERS = 30;
    
    public void Explode()
    {
        var size = Physics.OverlapSphereNonAlloc(transform.position, explodeRadius, _affectedColliders);
        if (size <= 0) { return; }

        var damageReceivers = GetReceivers(_affectedColliders);

        foreach (var damageReceiver in damageReceivers)
        {
            damageReceiver.ApplyDamage(maxDamage, gameObject, gameObject, new BaseDamage());
        }
        
        foreach (Collider coll in _affectedColliders)
        {
            Rigidbody rb = coll.GetComponent<Rigidbody>();

            if (rb != null)
            {
                rb.AddExplosionForce(explodePower, transform.position, explodeRadius, 3.0F);
            }
        }
    }


    private List<IDamageReceiver> GetReceivers(Collider[] colliders)
    {
        var receivers = new List<IDamageReceiver>();

        foreach (var coll in colliders)
        {
            if(!coll) { continue; }
            
            var receiver = coll.GetComponent<IDamageReceiver>();
            if (receiver != null)
            {
                receivers.Add(receiver);
            }
        }

        return receivers;
    }
}
