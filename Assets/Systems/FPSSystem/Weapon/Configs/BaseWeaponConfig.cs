﻿using UnityEngine;

[CreateAssetMenu(menuName = "SO/Weapons/Configs/BaseConfig")]
public class BaseWeaponConfig : ScriptableObject, IWeaponConfig
{
    [SerializeField] private int defaultAmmoAmount;
    [SerializeField] private int maxAmmoAmount;
    [SerializeField] private float fireDelayTime;
    [SerializeField] private float reloadTime;
    public int DefaultAmmoAmount => defaultAmmoAmount;
    public int MaxAmmoAmount => maxAmmoAmount;
    public float FireDelayTime => fireDelayTime;
    public float ReloadTime => reloadTime;
}
