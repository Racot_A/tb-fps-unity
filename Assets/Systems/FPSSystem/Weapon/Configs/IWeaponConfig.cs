﻿public interface IWeaponConfig
{
    int DefaultAmmoAmount { get; }
    int MaxAmmoAmount { get; }

    float FireDelayTime { get; }
    float ReloadTime { get; }
}
