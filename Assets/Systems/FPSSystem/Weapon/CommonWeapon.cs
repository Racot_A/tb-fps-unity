﻿public class CommonWeapon : BaseWeapon
{
    public override void OnShow()
    {
        
    }

    public override void OnHide()
    {
        
    }

    public override void OnShoot()
    {
        var bullet = GetFreeBullet();
        bullet.Activate(ShootPoint.position, ShootPoint.rotation);
    }
}
