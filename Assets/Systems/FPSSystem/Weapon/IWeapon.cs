﻿public interface IWeapon
{
    int Ammo { get; }

    void Show();
    void Hide();
    
    bool CanShoot();
    void Shoot();
}
