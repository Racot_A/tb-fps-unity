﻿using System.Collections.Generic;
using UnityEngine;

public abstract class BaseWeapon : MonoBehaviour, IWeapon
{
    [SerializeField] private BaseWeaponConfig config;
    [SerializeField] private Transform shootPoint;
    [SerializeField] private BaseBullet bulletPrefab;
    
    public Transform ShootPoint => shootPoint;
    
    private List<IBullet> _bulletsPool = new List<IBullet>();
    
    public int Ammo { get; private set; }

    private void Start()
    {
        Ammo = config.DefaultAmmoAmount;
    }

    
    public void Show()
    {
        gameObject.SetActive(true);
        OnShow();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        OnHide();
    }

    public bool CanShoot()
    {
        return true;
    }

    public void Shoot()
    {
        if(!CanShoot()) { return; }

        Ammo -= 1;

        OnShoot();
    }

    public IBullet GetFreeBullet()
    {
        foreach (var bullet in _bulletsPool)
        {
            if (!bullet.IsActive)
            {
                return bullet;
            }
        }

        var newBullet = Instantiate(bulletPrefab);
        newBullet.Deactivate();
        _bulletsPool.Add(newBullet);
        return newBullet;
    }

    public abstract void OnShow();
    public abstract void OnHide();
    public abstract void OnShoot();
}
