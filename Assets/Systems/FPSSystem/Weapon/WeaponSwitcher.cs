﻿using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitcher : MonoBehaviour
{
    [SerializeField] private int defaultWeaponIndex;

    [SerializeField] private List<BaseWeapon> availableWeapons = new List<BaseWeapon>();
    
    public IWeapon CurrentWeapon { get; private set; }
    public int CurrentWeaponIndex { get; private set; }


    private void Start()
    {
        if (defaultWeaponIndex >= 0)
        {
            SwitchToWeapon(defaultWeaponIndex);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown("1"))
        {
            SwitchToWeapon(0);
        }
        else if (Input.GetKeyDown("2"))
        {
            SwitchToWeapon(1);
        }
    }

    private void SwitchToWeapon(int weaponIndex)
    {
        if (weaponIndex < 0 || availableWeapons.Count == 0 || weaponIndex > availableWeapons.Count - 1) { return; }

        var nextWeapon = availableWeapons[weaponIndex];

        if(CurrentWeapon != null && CurrentWeapon.Equals(nextWeapon)) { return; }
        
        CurrentWeapon?.Hide();
        CurrentWeapon = nextWeapon;
        CurrentWeapon.Show();
    }
}
