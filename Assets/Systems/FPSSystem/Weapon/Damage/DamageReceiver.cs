﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ApplyDamageEvent : UnityEvent<float> { }

public class DamageReceiver : MonoBehaviour, IDamageReceiver
{
    [SerializeField] private float health = 10f;
    [SerializeField] private bool ignoreDamage;
    [Header("DEV")]
    [SerializeField] private float devTextDuration = 1f;

    [SerializeField] private ApplyDamageEvent damageEvent;
    [SerializeField] private UnityEvent destroyEvent;
    
    public Vector3 Position => transform.position;

    private Coroutine _textCor;
    private TextMesh _text;

    private void Awake()
    {
        var textPref = Resources.Load<TextMesh>("DamageReceiverText");

        _text = Instantiate(textPref);
    }

    public void ApplyDamage(float baseDamage, GameObject instigator, GameObject causer, IDamage damage)
    {
        if(ignoreDamage) { return; }
        
        health -= baseDamage;
        damageEvent?.Invoke(baseDamage);

        ShowText($"Damage:{baseDamage:F2} Health:{health:F2}");

        if (health <= 0)
        {
            AddScoresByDamage(damage);
            
            destroyEvent?.Invoke();
            
            Destroy(_text.gameObject);
            Destroy(gameObject);
        }
    }

    private static void AddScoresByDamage(IDamage damage)
    {
        if (damage is CommonDamage)
        {
            Core.Instance.References.Score.AddScore(1);
        }
        else if (damage is ExplosiveDamage)
        {
            Core.Instance.References.Score.AddScore(2);
        }
    }

    private void ShowText(string text)
    {
        _text.transform.rotation = Quaternion.LookRotation( _text.transform.position - SceneHandler.MainCamera.transform.position);
        
        _text.transform.position = transform.position + new Vector3(0, 0.7f, 0);
        _text.text = text;
        _text.gameObject.SetActive(true);

        if (_textCor != null)
        {
            StopCoroutine(_textCor);
            _textCor = null;
        }
        _textCor = StartCoroutine(WaitCor(devTextDuration, () => { _text.gameObject.SetActive(false); }));
    }

    IEnumerator WaitCor(float wait, Action done)
    {
        yield return new WaitForSeconds(wait);
        done?.Invoke();
    }
}
