﻿using UnityEngine;

public interface IDamageReceiver
{
    Vector3 Position { get; }
    void ApplyDamage(float baseDamage, GameObject instigator, GameObject causer, IDamage damage);
}
