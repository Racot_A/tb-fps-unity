﻿using UnityEngine;

public class PlayerController : BaseCharacterController
{
    public MouseLook MouseLook => _mouseLook;
    
    [SerializeField] private float walkSpeed = 1;
    [SerializeField] private float runSpeed = 4;
    [SerializeField] [Range(0f, 1f)] private float runstepLenghten = 0.7f;
    [SerializeField] private float jumpSpeed = 5;
    [SerializeField] private float stickToGroundForce = 10;
    [SerializeField] private float gravityMultiplier = 2;
    [SerializeField] private float stepInterval = 3;
    
    private Camera _camera;
    public bool _jump;
    private bool _isWalking;
    private bool _isRunning;
    private float _yRotation;
    private Vector2 _input;
    private Vector3 _moveDir = Vector3.zero;
    private CharacterController _characterController;
    private bool _previouslyGrounded;
    private float _stepCycle;
    private float _nextStep;
    private bool _jumping;

    private float _speed;
    private Vector2 _moveInput;

    private PlayerCharacter _playerCharacter;
    private CharacterController _controller;
    private MouseLook _mouseLook;

    

    public override void Start()
    {
        base.Start();
        //Initialize references
        _controller = GetComponent<CharacterController>();
        _mouseLook = GetComponentInChildren<MouseLook>();
        
        _playerCharacter = GetComponent<PlayerCharacter>();
        _characterController = GetComponent<CharacterController>();
        _camera = GetComponentInChildren<Camera>(true);
        
        //Initialize default values
        _mouseLook.SetCursorLock(true);
        
        _stepCycle = 0f;
        _nextStep = _stepCycle / 2f;
        _jumping = false;
        
        _isWalking = true;
        _isRunning = false;

        UpdateSettingsFromSaves();
        var playerSaveData = Core.Instance.References.Saves.DataContainer.Player;
        playerSaveData.ModifyEvent += UpdateSettingsFromSaves;

        //Initialize Input
        var playerInput = _playerCharacter.GetComponent<GamePlayerInput>();
        playerInput.MovementPerformed += MovementOnPerformed;
        playerInput.MovementCancelled += MovementOnCancelled;
        playerInput.LookPerformed += OnLookInput;
        playerInput.JumpPerformed += JumpOnPerformed;
        playerInput.RunStarted += RunOnPerformed;
        playerInput.RunPerformed += RunOnCancelled;
        
        playerInput.SwitchMenuPerformed += SwitchGameMenu;

        playerInput.MainActionPerformed += MainAction;


    }
    
    private void OnDestroy()
    {
        var playerSaveData = Core.Instance.References.Saves.DataContainer.Player;
        playerSaveData.ModifyEvent -= UpdateSettingsFromSaves;
        
        var playerInput = _playerCharacter.GetComponent<GamePlayerInput>();
        playerInput.MovementPerformed -= MovementOnPerformed;
        playerInput.MovementCancelled -= MovementOnCancelled;
        playerInput.LookPerformed -= OnLookInput;
        playerInput.JumpPerformed -= JumpOnPerformed;
        playerInput.RunStarted -= RunOnPerformed;
        playerInput.RunPerformed -= RunOnCancelled;
        
        playerInput.SwitchMenuPerformed -= SwitchGameMenu;
        
        //Player Character flow
        playerInput.MainActionStarted -= MainAction;
    }  

    private void MainAction()
    {
        if (InputLocked) { return; }

        _playerCharacter.MainHandsAction();
    }
    
    void OnLookInput(Vector2 lookDir)
    {
        _mouseLook.UpdateLookInput(lookDir);
    }
    
    private void MovementOnPerformed(Vector2 moveDir)
    {
        _moveInput = moveDir;
        UpdateInput();
    }
    
    private void MovementOnCancelled(Vector2 moveDir)
    {
        _moveInput = Vector2.zero;
    }
    
    private void RunOnPerformed()
    {
        _isWalking = false;
        _isRunning = true;
    }

    private void RunOnCancelled()
    {
        _isWalking = true;
        _isRunning = false;
    }

    private void JumpOnPerformed()
    {
        // the jump state needs to read here to make sure it is not missed
        if (!_jump)
        {
            _jump = true;
        }
    }
    
    
    private void SwitchGameMenu()
    {
        var sceneHandler = SceneHandler.Current as GameSceneHandler;
        sceneHandler.SwitchPause();
    }
    
    private void UpdateSettingsFromSaves()
    {
        var playerSaveData = Core.Instance.References.Saves.DataContainer.Player;
        walkSpeed = playerSaveData.MovementSpeed;
        runSpeed = playerSaveData.RunningSpeed;
    }

    private void Update()
    {
        if (!_previouslyGrounded && _controller.isGrounded)
        {
            PlayLandingSound();
            _nextStep = _stepCycle + .5f;
            _moveDir.y = 0f;
            _jumping = false;
        }

        if (!_controller.isGrounded && !_jumping && _previouslyGrounded)
        {
            _moveDir.y = 0f;
        }

        _previouslyGrounded = _characterController.isGrounded;
    }
    
    private void FixedUpdate()
    {
        //Debug.LogError("InputLocked:" + InputLocked);
        if (InputLocked)
        {
            return;
        }
        // Always move along the camera forward as it is the direction that it being aimed at
        UpdateInput();
        
        RotateView();

        Vector3 desiredMove = transform.forward * _input.y + transform.right * _input.x;

        //Get a normal for the surface that is being touched to move along it
        RaycastHit hitInfo;
        Physics.SphereCast(transform.position, _characterController.radius, Vector3.down, out hitInfo,
            _characterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
        desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

        _moveDir.x = desiredMove.x * _speed;
        _moveDir.z = desiredMove.z * _speed;
        
        if (_characterController.isGrounded)
        {
            _moveDir.y = -stickToGroundForce;

            if (_jump)
            {
                _moveDir.y = jumpSpeed;
                PlayJumpSound();
                _jump = false;
                _jumping = true;
            }
        }
        else
        {
            if (!GravityDisabled)
            {
                _moveDir += Physics.gravity * gravityMultiplier * Time.fixedDeltaTime;
            }
        }
        
        _controller.Move(_moveDir * Time.fixedDeltaTime);
        
        ProgressStepCycle(_speed);
    }

    private void UpdateInput()
    {
        float horizontal = _moveInput.x;
        float vertical = _moveInput.y;
        
        // Set the desired speed to be walking or running
        _speed = _isRunning ? runSpeed : walkSpeed;

        _input = new Vector2(horizontal, vertical);

        // Normalize input if it exceeds 1 in combined length:
        if (_input.sqrMagnitude > 1)
        {
            _input.Normalize();
        }
    }
    
    private void PlayLandingSound()
    {
        
    }

    private void ProgressStepCycle(float speed)
    {
        if (_characterController.velocity.sqrMagnitude > 0 && (_input.x != 0 || _input.y != 0))
        {
            _stepCycle +=
                (_characterController.velocity.magnitude + (speed * (_isWalking ? 1f : runstepLenghten))) *
                Time.fixedDeltaTime;
        }

        if (!(_stepCycle > _nextStep))
        {
            return;
        }

        _nextStep = _stepCycle + stepInterval;

        PlayFootStepAudio();
    }

    private void PlayJumpSound()
    {
        
    }

    private void PlayFootStepAudio()
    {
        if (!_characterController.isGrounded) { return; }
    }

    private void RotateView()
    {
        if (LookLocked)
        {
            _mouseLook.LookRotation(_camera.transform);
        }
        else
        {
            _mouseLook.LookRotation(transform, _camera.transform);
        }
    }

    protected override void OnInputLocked()
    {
        if (_controller != null)
        {
            _controller.enabled = false;
        }
    }

    protected override void OnInputUnlocked()
    {
        if (_controller != null)
        {
            _controller.enabled = true;
        }
    }
}
