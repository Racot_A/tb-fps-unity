﻿using UnityEngine;

public class MouseLook : MonoBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private bool isEnabled = true;

    [SerializeField] private float xSensitivity = 2f;
    [SerializeField] private float ySensitivity = 2f;
    [SerializeField] private bool clampVerticalRotation = true;
    [SerializeField] private float minimumX = -90F;
    [SerializeField] private float maximumX = 90F;
    [SerializeField] private bool smooth;
    [SerializeField] private float smoothTime = 13;

    private Quaternion _characterTargetRot;
    private Quaternion _cameraTargetRot;

    private Vector2 _curRotation;
    private Vector2 _lookInput;
    private Vector2 _targetInput;

    private float _normalizedTurningSpeed;

    private void Awake()
    {
        var character = GetComponentInParent<PlayerCharacter>();
        _characterTargetRot = character.transform.localRotation;
        _cameraTargetRot = camera.transform.localRotation;
    }

    public void UpdateLookInput(Vector2 lookInput)
    {
        _lookInput = lookInput;
        _lookInput.x = Mathf.Clamp(_lookInput.x, -1f, 1f);
        _lookInput.y = Mathf.Clamp(_lookInput.y, -1f, 1f);
    }

    public void LookRotation(Transform character, Transform camera)
    {
        //Get Input axises
        _curRotation = new Vector2(Input.GetAxis("Mouse X") * xSensitivity, Input.GetAxis("Mouse Y") * ySensitivity);

        var xRot = _curRotation.x;
        var yRot = _curRotation.y;

        //Calculate Rotations
        _characterTargetRot *= Quaternion.Euler(0f, xRot, 0f);
        _cameraTargetRot *= Quaternion.Euler(-yRot, 0f, 0f);

        if (clampVerticalRotation)
        {
            _cameraTargetRot = ClampRotationAroundXAxis(_cameraTargetRot);
        }

        if (smooth)
        {
            character.localRotation = Quaternion.Slerp(character.localRotation, _characterTargetRot,
                smoothTime * Time.deltaTime);
            if (isEnabled)
            {
                camera.localRotation = Quaternion.Slerp(camera.localRotation, _cameraTargetRot,
                    smoothTime * Time.deltaTime);
            }
        }
        else
        {
            character.localRotation = _characterTargetRot;
            if (isEnabled)
            {
                camera.localRotation = _cameraTargetRot;
            }
        }
    }

    public void LookRotation(Transform camera)
    {
        _curRotation = new Vector2(Input.GetAxis("Mouse X") * xSensitivity, Input.GetAxis("Mouse Y") * ySensitivity);

        var xRot = _curRotation.x;
        var yRot = _curRotation.y;
        
        _cameraTargetRot *= Quaternion.Euler(-yRot, xRot, 0f);

        if (clampVerticalRotation)
        {
            _cameraTargetRot = ClampRotationAroundXAxis(_cameraTargetRot);
        }

        if (smooth)
        {
            if (isEnabled)
            {
                camera.localRotation = Quaternion.Slerp(camera.localRotation, _cameraTargetRot,
                    smoothTime * Time.deltaTime);
            }
        }
        else
        {
            if (isEnabled)
            {
                camera.localRotation = _cameraTargetRot;
            }
        }
    }
    
    public void SetCursorLock(bool locked)
    {
        Cursor.lockState = locked ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !locked;
    }

    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, minimumX, maximumX);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }
}