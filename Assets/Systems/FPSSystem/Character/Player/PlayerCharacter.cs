﻿using UnityEngine;

public class PlayerCharacter : BaseCharacter
{
    [SerializeField] private WeaponSwitcher weaponSwitcher;
    
    public void MainHandsAction()
    {
        var curWeapon = weaponSwitcher.CurrentWeapon;
        curWeapon?.Shoot();
    }
}
