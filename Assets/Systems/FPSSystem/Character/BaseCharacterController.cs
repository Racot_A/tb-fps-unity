﻿using UnityEngine;

public abstract class BaseCharacterController : MonoBehaviour
{
    
    protected bool InputLocked { get; private set; }
    protected bool MovementLocked { get; private set; }
    protected bool LookLocked { get; private set; }
    
    protected bool GravityDisabled { get; private set; }


    public virtual void Start()
    {
        //Implemented by childs 
    }
    
    public void LockInput()
    {
        LockMovement();
        LockLook();
        InputLocked = true;

        OnInputLocked();
    }

    public void UnlockInput()
    {
        UnlockMovement();
        UnlockLook();
        InputLocked = false;
        OnInputUnlocked();
    }
    
    
    public void LockMovement()
    {
        MovementLocked = true;
    }

    public void UnlockMovement()
    {
        MovementLocked = false;
    }
    
    public void LockLook()
    {
        LookLocked = true;
    }

    public void UnlockLook()
    {
        LookLocked = false;
    }

    protected abstract void OnInputLocked();
    protected abstract void OnInputUnlocked();
}
