﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    [SerializeField] private TriggerVolume triggerVolume;

    void Start()
    {
        triggerVolume.TriggerEnterEvent.AddListener(TeleporterEnter);
    }

    private void TeleporterEnter(Collider coll)
    {
        var player = coll.GetComponent<PlayerCharacter>();
        if (player)
        {
            return;
        }
        coll.gameObject.SetActive(false);
    }
}
