﻿using System.Collections;
using UnityEngine;

public class PrimitivesSpawner : MonoBehaviour
{
    [SerializeField] private float spawnDelay = 1;

    private Coroutine _spawnCor;
    
    void Start()
    {
        _spawnCor = StartCoroutine(SpawnLoop());
    }

    IEnumerator SpawnLoop()
    {
        while (isActiveAndEnabled)
        {
            yield return new WaitForSeconds(spawnDelay);

            var primitive = CreateRandomPrimitive();
            primitive.AddComponent<DamageReceiver>();
        }
    }

    private GameObject CreateRandomPrimitive()
    {
        var randValue = UnityEngine.Random.Range(0, 4);

        GameObject primitive = null;

        switch (randValue)
        {
            case 0: 
                primitive = GameObject.CreatePrimitive(PrimitiveType.Cube);
                break;
            case 1: 
                primitive = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                break;
            case 2: 
                primitive = GameObject.CreatePrimitive(PrimitiveType.Capsule);
                break;
            case 3: 
                primitive = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
                break;
            default: 
                primitive = GameObject.CreatePrimitive(PrimitiveType.Cube);
                break;
        }

        primitive.AddComponent<Rigidbody>();
        var scaleValue = UnityEngine.Random.value;
        primitive.transform.localScale = new Vector3(scaleValue, scaleValue, scaleValue);
        primitive.transform.position = transform.position + UnityEngine.Random.insideUnitSphere + primitive.transform.localScale;//

        return primitive;
    }
}
