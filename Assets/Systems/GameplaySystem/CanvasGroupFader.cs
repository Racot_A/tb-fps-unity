﻿using System.Collections;
using UnityEngine;

public class CanvasGroupFader : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private float fadeDuration = 1;
    [SerializeField] private bool fadeOutInStart;
    [SerializeField] private float delay;
    
    private bool _isFading;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(delay);
        
        if (fadeOutInStart)
        {
            FadeIOut();
        }
    }

    public void FadeIn()
    {
        if (!_isFading)
        {
            StartCoroutine(Fade(1));
        }
    }
    
    public void FadeIOut()
    {
        if (!_isFading)
        {
            StartCoroutine(Fade(0));
        }
    }

    private IEnumerator Fade(float finalAlpha)
    {
        var fadeCanvasGroup = canvasGroup; 
        _isFading = true;
        fadeCanvasGroup.blocksRaycasts = true;

        var fadeSpeed = Mathf.Abs((fadeCanvasGroup.alpha - finalAlpha) / fadeDuration);
        while (!Mathf.Approximately(fadeCanvasGroup.alpha, finalAlpha))
        {
            fadeCanvasGroup.alpha = Mathf.MoveTowards(fadeCanvasGroup.alpha, finalAlpha, fadeSpeed * Time.deltaTime);
            yield return null;
        }

        _isFading = false;
        fadeCanvasGroup.blocksRaycasts = false;
    }
}
